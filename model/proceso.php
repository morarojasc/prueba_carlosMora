<?php
class proceso
{
	private $pdo;

    public $id;
    public $numero_proceso;
    public $descripcion;
    public $fecha_creacion;
    public $sede;
    public $presupuesto;

	public function __CONSTRUCT()
	{
		//constructor para validar conexion con la base de datos
		try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Listar()
	//Mostrar Todos los procesos
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM procesos ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	//Mostrar procesos por id
	{
		try
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM procesos WHERE id = ?");

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Detalle($id)
	{
		try
		{
			$stm = $this->pdo
								->prepare("SELECT * FROM procesos WHERE id = ?");

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function filtro($id)
	{
		try
		{
			$stm = $this->pdo
								->prepare("SELECT * FROM procesos WHERE fecha = ?");

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM procesos WHERE id = ?");

			$stm->execute(array($id));
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)

	{
		try
		{
			$sql = "UPDATE procesos SET

						descripcion          = ?,
						fecha_creacion        = ?,
                        sede        = ?,
                        presupuesto        = ?

				    WHERE id = ?";
$fecha=date("Y"."/"."m"."/"."d");
			$this->pdo->prepare($sql)
			     ->execute(
				    array(

                        $data->descripcion,
                        $data->fecha_creacion=$fecha,
                         $data->sede,
                        $data->presupuesto,
                        $data->id
					)
				);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Registrar(proceso $data)
	{
		try
		{
		$sql = "INSERT INTO procesos (descripcion,fecha_creacion,sede,presupuesto)
		        VALUES ( ?, ?, ?, ?)";
	$fecha=date("Y"."/"."m"."/"."d");

		$this->pdo->prepare($sql)
		     ->execute(
				array(


                    $data->descripcion,
                    $data->fecha_creacion=$fecha,
                    $data->sede,
                     $data->presupuesto

                )
			);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}
