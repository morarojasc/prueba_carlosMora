<h1 class="page-header">
    <?php echo $proceso->id != null ? $proceso->numero_proceso : 'Nuevo Proceso'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=proceso" style="color:#6F282C">Inicio</a></li>
  <li class="active"><?php echo $proceso->id != null ? $proceso->numero_proceso : 'Nuevo Proceso'; ?></li>
</ol>

<form id="frm" action="?c=proceso&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $proceso->id; ?>" />
      <div class="form-group">

    <div class="form-group">
        <label>Descripcion</label>
        <input type="text" name="descripcion" value="<?php echo $proceso->descripcion; ?>" class="form-control" placeholder="Ingrese descripcion" required>
    </div>

    <div class="form-group">
        <label>Sede</label>
        <select name="sede"  class="form-control">
       <option value=" " selected="selected">-Seleccione-</option>
       <option value="Bogota">Bogota</option>
       <option value="Mexico">Mexico</option>
       <option value="Peru">Peru</option>
   </select>

    </div>
     <div class="form-group">
        <label>Presupuesto</label>
        <input type="text" name="presupuesto" onkeypress="return validaFormato(event, this);"  value="<?php echo $proceso->presupuesto; ?>" class="form-control" placeholder="Ingrese presupuesto" required>
    </div>


    <hr />

    <div class="text-right">
        <button class="btn btn-warning"  >Guardar</button>
    </div>
</form>

<br>
<br>
<script>
    $(document).ready(function(e){
        $("#frm").submit(function(){
	go = confirm('Proceso Generado <?php echo $n_id;  ?>');
            return $(this).validate();
        });
    })
</script>

<script>
//Validacion de Numeros
function validaFormato(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    if (key == 8) return true
    if (key > 47 && key < 58) {
      if (field.value == "") return true
      regexp = /.[0-9]{15}$/
      return !(regexp.test(field.value))
    }
    if (key == 46) {
      if (field.value == "") return false
      regexp = /^[0-9]+$/
      return regexp.test(field.value)
    }
    return false
  }
</script>
