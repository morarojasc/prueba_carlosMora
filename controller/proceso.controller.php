<?php
require_once 'model/proceso.php';

class procesoController{

    private $model;

    public function __CONSTRUCT(){
        $this->model = new proceso();
    }

    public function Index(){
        require_once 'view/header.php';
        require_once 'view/vista/proceso.php';
        require_once 'view/vista/footer.php';

    }

    public function Crud(){
        $proceso = new proceso();

        if(isset($_REQUEST['id'])){
            $proceso = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/header.php';
        require_once 'view/vista/proceso-editar.php';
        require_once 'view/vista/footer.php';

    }

    public function Detalle(){
        $proceso = new proceso();

        if(isset($_REQUEST['id'])){
            $proceso = $this->model->Detalle($_REQUEST['id']);
        }

        require_once 'view/header.php';
        require_once 'view/vista/proceso-detalle.php';
        require_once 'view/vista/footer.php';

    }

    public function Consultar(){
        $proceso = new proceso();

        if(isset($_REQUEST['id'])){
            $proceso = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/header.php';
        require_once 'view/vista/proceso-consultar.php';
        require_once 'view/vista/footer.php';

    }

    public function Guardar(){
        $proceso = new proceso();

        $proceso->id = $_REQUEST['id'];

        $proceso->descripcion = $_REQUEST['descripcion'];

        $proceso->sede = $_REQUEST['sede'];
        $proceso->presupuesto = $_REQUEST['presupuesto'];


        $proceso->id > 0
            ? $this->model->Actualizar($proceso)
            : $this->model->Registrar($proceso);

            require_once 'view/header.php';
            require_once 'view/vista/proceso-consultar.php';
            require_once 'view/vista/footer.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php');
    }
}
